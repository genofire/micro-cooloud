# Micro Cooloud
a litte tool to test a Collabora Instance

## Endpoints
- GET `/` - list all files
- GET `/upload` - formular for POST
- POST `/upload`
- GET `/open/{path}` - formular to Open Document in Collabora
- internals for Collabora
  - GET `/cool/{path}` - get FileInfo (CheckFileInfo)
  - GET `/cool/{path}/contents` get File
  - POST `/cool/{path}/contents` save File (PutRelativeFile)

## TODO
- Check File is really in Webroot (../)
- Authentification AccessToken
- from browse direct to collabora
