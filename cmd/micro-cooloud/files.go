package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"go.uber.org/zap"

	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
)

type Action struct {
	XMLName xml.Name `xml:"action"`
	Default bool     `xml:"default,attr"`
	Name    string   `xml:"name,attr"`
	Ext     string   `xml:"ext,attr"`
	URLSrc  string   `xml:"urlsrc,attr"`
}

type App struct {
	XMLName    xml.Name `xml:"app"`
	Name       string   `xml:"name,attr"`
	FavIconURL string   `xml:"favIconUrl,attr"`
	Actions    []Action `xml:"action"`
}

type NetZone struct {
	XMLName xml.Name `xml:"net-zone"`
	Name    string   `xml:"name,attr"`
	Apps    []App    `xml:"app"`
}

type DiscoveryXML struct {
	XMLName  xml.Name  `xml:"wopi-discovery"`
	NetZones []NetZone `xml:"net-zone"`
}

func discovery(server string, log *zap.Logger) string {
	resp, err := http.Get(fmt.Sprintf("%s/hosting/discovery", server))
	if err != nil {
		log.Panic("discovery collabora frontend", zap.Error(err))
		return ""
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Panic("read collabora discovery", zap.Error(err))
		return ""
	}

	var discoveryData DiscoveryXML
	xml.Unmarshal(body, &discoveryData)

	srcurl := ""
find:
	for _, zone := range discoveryData.NetZones {
		for _, app := range zone.Apps {
			if app.Name == "text/plain" {
				if len(app.Actions) > 0 {
					srcurl = app.Actions[0].URLSrc
					break find
				}
			}
		}
	}
	return srcurl
}

func bindFile(r *gin.Engine, log *zap.Logger, config *Config) {
	// save file: PutRelativeFile
	r.POST("/cool/:path/contents", func(c *gin.Context) {
		fullpath := filepath.Join(config.Webroot, c.Param("path"))
		if c.Request.Body == nil {
			return
		}
		bodyBytes, _ := ioutil.ReadAll(c.Request.Body)
		ioutil.WriteFile(fullpath, bodyBytes, 0644)
		c.JSON(http.StatusOK, gin.H{})
	})
	// get file
	r.GET("/cool/:path/contents", func(c *gin.Context) {
		fullpath := filepath.Join(config.Webroot, c.Param("path"))
		file, _ := ioutil.ReadFile(fullpath)
		c.Data(http.StatusOK, "application/octet-stream", file)
	})
	// CheckFileInfo
	r.GET("/cool/:path", func(c *gin.Context) {
		fullpath := filepath.Join(config.Webroot, c.Param("path"))
		f, _ := os.Stat(fullpath)

		userID := c.DefaultQuery("access_token", "1")

		c.JSON(http.StatusOK, gin.H{
			"BaseFileName":     filepath.Base(fullpath),
			"Size":             f.Size(),
			"UserId":           userID,
			"UserFriendlyName": fmt.Sprintf("UserName-%s", userID),
			"UserCanWrite":     c.DefaultQuery("can-write", "true"),
		})
	})
	r.GET("/open/:path", func(c *gin.Context) {
		colURL := fmt.Sprintf("%sWOPISrc=", discovery(config.Collabora, log))

		// build document URL
		ownURL := c.Request.URL
		ownURL.Scheme = config.Scheme
		ownURL.Host = c.Request.Host
		ownURL.Path = fmt.Sprintf("cool/%s", c.Param("path"))
		wopisrc := ownURL.String()

		c.HTML(http.StatusOK, TemplateOpen, gin.H{
			"CollaboraURL": colURL,
			"path":         wopisrc,
			"UserId":       c.DefaultQuery("user-id", "1"),
		})
	})
	r.GET("/upload", func(c *gin.Context) {
		c.HTML(http.StatusOK, TemplateUpload, gin.H{})
	})
	r.POST("/upload", func(c *gin.Context) {
		file, err := c.FormFile("file")
		if err != nil {
			c.String(http.StatusBadRequest, "get form err: %s", err.Error())
			return
		}

		path := filepath.Join(config.Webroot, filepath.Base(file.Filename))
		if err := c.SaveUploadedFile(file, path); err != nil {
			c.String(http.StatusBadRequest, "upload file err: %s", err.Error())
			return
		}

		c.String(http.StatusOK, "File %s uploaded successfully", file.Filename)
	})

	if config.Webroot != "" {
		config.WebrootFS = static.LocalFile(config.Webroot, true)
	}
	r.Use(func(c *gin.Context) {
		if config.WebrootIndexEnabled {
			_, err := config.WebrootFS.Open(c.Request.URL.Path)
			if err != nil {
				c.FileFromFS("/", config.WebrootFS)
				return
			}
		}
		c.FileFromFS(c.Request.URL.Path, config.WebrootFS)
	})
}
