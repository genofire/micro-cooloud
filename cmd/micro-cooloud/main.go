package main

import (
	"flag"
	"net/http"
	"path/filepath"
	"strings"

	//logging
	"go.uber.org/zap"

	// config loading
	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/json"
	"github.com/knadh/koanf/parsers/toml"
	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/env"
	"github.com/knadh/koanf/providers/file"

	"github.com/gin-gonic/gin"
	// acme
	"github.com/gin-gonic/autotls"
	"golang.org/x/crypto/acme/autocert"
)

type Config struct {
	Log                 *zap.Config     `config:"log"`
	Listen              string          `config:"listen"`
	AccessLog           bool            `config:"access_log"`
	WebrootIndexEnabled bool            `config:"webroot_index_enabled"`
	Webroot             string          `config:"webroot"`
	WebrootFS           http.FileSystem `config:"-"`
	ACME                struct {
		Enabled bool     `config:"enabled"`
		Domains []string `config:"domains"`
		Cache   string   `config:"cache"`
	} `config:"acme"`
	Collabora string `config:"collabora"`
	Scheme    string `config:"scheme"`
}

var configExtParser = map[string]koanf.Parser{
	".json": json.Parser(),
	".toml": toml.Parser(),
	".yaml": yaml.Parser(),
	".yml":  yaml.Parser(),
}

func runWebserver(config *Config, log *zap.Logger) error {
	gin.EnableJsonDecoderDisallowUnknownFields()
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	// catch crashed
	r.Use(gin.Recovery())

	if config.AccessLog {
		r.Use(gin.Logger())
		log.Debug("request logging enabled")
	}

	bindMetrics(r)
	bindTemplate(r, log)
	// file must be at last
	bindFile(r, log, config)

	if config.ACME.Enabled {
		if config.Listen != "" {
			log.Panic("For ACME / Let's Encrypt it is not possible to set `listen`")
		}
		m := autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(config.ACME.Domains...),
			Cache:      autocert.DirCache(config.ACME.Cache),
		}
		return autotls.RunWithManager(r, &m)
	}
	return r.Run(config.Listen)
}

func main() {
	configPath := "config.toml"

	log, _ := zap.NewProduction()

	flag.StringVar(&configPath, "c", configPath, "path to configuration file")
	flag.Parse()

	k := koanf.New("/")

	if configPath != "" {
		fileExt := filepath.Ext(configPath)
		parser, ok := configExtParser[fileExt]
		if !ok {
			log.Panic("unsupported file extension:",
				zap.String("config-path", configPath),
				zap.String("file-ext", fileExt),
			)
		}
		if err := k.Load(file.Provider(configPath), parser); err != nil {
			log.Panic("load file config:", zap.Error(err))
		}
	}

	if err := k.Load(env.Provider("COOLOUD_", "/", func(s string) string {
		return strings.Replace(strings.ToLower(
			strings.TrimPrefix(s, "COOLOUD_")), "__", "/", -1)
	}), nil); err != nil {
		log.Panic("load env:", zap.Error(err))
	}

	config := &Config{}
	if err := k.UnmarshalWithConf("", &config, koanf.UnmarshalConf{Tag: "config"}); err != nil {
		log.Panic("reading config", zap.Error(err))
	}
	if config.Log != nil {
		l, err := config.Log.Build()
		if err != nil {
			log.Panic("generate logger from config", zap.Error(err))
		}
		log = l
	}
	if err := runWebserver(config, log); err != nil {
		log.Fatal("crash webserver", zap.Error(err))
	}
}
