package main

import (
	"html/template"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

const (
	TemplateOpen   = "open.html"
	TemplateUpload = "upload.html"
)

var templatesMap = map[string]string{
	TemplateUpload: `
		<!doctype html>
		<html lang="en">
			<head>
				<meta charset="utf-8">
				<title>File upload</title>
			</head>
			<body>
			<h1>Upload File</h1>

			<form action="/upload" method="post" enctype="multipart/form-data">
				Files: <input type="file" name="file"><br><br>
				<input type="submit" value="Submit">
			</form>
			</body>
		</html>
	`,
	TemplateOpen: `
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html lang="en">
			<body>
				<form action="{{ .CollaboraURL }}{{ .path }}" method="POST" target="collabora-online-viewer">
					<label for="collabora-online-server">Collabora Online Server</label>
					<input type="hidden" name="access_token" value="{{ .UserId }}"/>
					<input type="submit" name="submit" value="Load Collabora Online" />
				</form>
				
				<iframe id="collabora-online-viewer" name="collabora-online-viewer" style="width:100%;height:80%;position:absolute;">
				</iframe>
			
			</body>
		</html>
	`,
}

func bindTemplate(r *gin.Engine, log *zap.Logger) {
	templates := template.New("")
	for file, temp := range templatesMap {
		t, err := templates.New(file).Parse(temp)
		if err != nil {
			log.Panic("failed parse template:",
				zap.String("template", file),
				zap.Error(err),
			)
		}
		templates = t
	}
	r.SetHTMLTemplate(templates)
}
