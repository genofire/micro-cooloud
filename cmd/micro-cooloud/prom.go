package main

import (
	"strings"

	"github.com/gin-gonic/gin"
	// gin-prometheus
	"github.com/chenjiandongx/ginprom"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const NAMESPACE = "cooloud"

var (
	VERSION = "develop"
	UP      = func() bool { return true }
)

func bindMetrics(r *gin.Engine) {
	r.Use(ginprom.PromMiddleware(&ginprom.PromOpts{
		EndpointLabelMappingFn: func(c *gin.Context) string {
			url := c.Request.URL.Path
			for _, p := range c.Params {
				url = strings.Replace(url, p.Value, ":"+p.Key, 1)
			}
			return url
		},
	}))

	prometheus.MustRegister(prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace:   NAMESPACE,
			Name:        "up",
			Help:        "is current version of service running",
			ConstLabels: prometheus.Labels{"version": VERSION},
		},
		func() float64 {
			if UP() {
				return 1
			}
			return 0
		},
	))
	r.GET("/metrics", ginprom.PromHandler(promhttp.Handler()))
}
